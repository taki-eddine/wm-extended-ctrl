#!/bin/bash
shopt -s expand_aliases # enable alias in script.

#***************************************************************************************************
source '/usr/share/brush/util/argument_helper.sh'
source '/usr/share/brush/util/log.sh'
colorize
source '/usr/share/brush/math.sh'
#***************************************************************************************************

alias evalp='eval echo'

#**************************************************************************************************#
declare -A panel=([top]=0
                  [right]=0
                  [bottom]=25
                  [left]=0
                 )


#**************************************************************************************************#


# monitors.
nmonitors=0

declare -A monitors

while read monitor_xrandr; do
    brush::log.d "main" "monitor_xrandr = ${monitor_xrandr}"
    declare -A monitor_properties="( $(  echo ${monitor_xrandr}                                                                \
                                       | sed -r 's:([0-9]+)x([0-9]+)\+([0-9]+)\+([0-9]+):[width]=\1 [height]=\2 [x]=\3 [y]=\4:') )"

    monitors[${nmonitors}, width]=${monitor_properties[width]}
    monitors[${nmonitors}, half_width]=$(( ${monitors[${nmonitors}, width]} / 2 ))

    monitors[${nmonitors}, height]=${monitor_properties[height]}
    monitors[${nmonitors}, half_height]=$(( ${monitors[${nmonitors}, height]} / 2 ))

    monitors[${nmonitors}, x]=${monitor_properties[x]}
    monitors[${nmonitors}, y]=${monitor_properties[y]}

    monitors[${nmonitors}, top]=$(( ${monitors[${nmonitors}, y]} ))
    monitors[${nmonitors}, right]=$(( ${monitors[${nmonitors}, x]} + ${monitors[${nmonitors}, width]} ))
    monitors[${nmonitors}, bottom]=$(( ${monitors[${nmonitors}, y]} + ${monitors[${nmonitors}, height]} ))
    monitors[${nmonitors}, left]=$(( ${monitors[${nmonitors}, x]} ))

    (( ++nmonitors ))
done < <(xrandr | grep -Po '(?<= connected | primary )[0-9x+]+')

for (( i=0; i < ${nmonitors}; ++i )); do
    brush::log.d "wm-ctrl.global" "monitors[${i}] = {
                                [width]       = ${monitors[${i}, width]}
                                [half_width]  = ${monitors[${i}, half_width]}
                                [height]      = ${monitors[${i}, height]}
                                [half_height] = ${monitors[${i}, half_height]}
                                [x]           = ${monitors[${i}, x]}
                                [y]           = ${monitors[${i}, y]}
                                [top]         = ${monitors[${i}, top]}
                                [right]       = ${monitors[${i}, right]}
                                [bottom]      = ${monitors[${i}, bottom]}
                                [left]        = ${monitors[${i}, left]}
                            }"
done

############################################################

declare -A monitors_map

sorted_monitor=($(sort -t '=' -k '2' <(
                   for (( i = 0; i < nmonitors; ++i )); do
                       printf "${i} "
                   done
               )))

brush::log.d "main" "sorted_monitor = { ${sorted_monitor[@]} }"

for (( i = 0, n = nmonitors / 2; i < n; ++i )); do
    monitor_0=${sorted_monitor[${i}]}
    monitor_1=${sorted_monitor[((${i} + 1))]}

    monitors_map[${monitor_0}, right]=${monitor_1}
    monitors_map[${monitor_1}, left]=${monitor_0}
done

for (( i=0; i < ${nmonitors}; ++i )); do
    brush::log.d "wm-ctrl.global" "monitors[${i}] = {
                                [right] = ${monitors_map[${i}, right]}
                                [left]  = ${monitors_map[${i}, left]}
                            }"
done


#**************************************************************************************************#


############################################################
function window.getX() {
    local window_id=${1}
    local x_absolute=$(xwininfo -id ${window_id} | awk '/Absolute upper-left X:/ { print $4 }')
    local x_relative=$(xwininfo -id ${window_id} | awk '/Relative upper-left X:/ { print $4 }')
    echo $(( ${x_absolute} - ${x_relative} ))
}


############################################################
function window.getY() {
    local window_id=${1}
    local y_absolute=$(xwininfo -id ${window_id} | awk '/Absolute upper-left Y:/ { print $4 }')
    local y_relative=$(xwininfo -id ${window_id} | awk '/Relative upper-left Y:/ { print $4 }')
    echo $(( ${y_absolute} - ${y_relative} ))
}


############################################################
function window.getWidth() {
    local window_id=${1}
    local width=$(xwininfo -id ${window_id} | awk '/Width:/ { print $2 }')
    local x_relative=$(xwininfo -id ${window_id} | awk '/Relative upper-left X:/ { print $4 }')

    echo $(( ${width} + (${x_relative} * 2) ))
}


############################################################
function window.getHeight() {
    local window_id=${1}
    local height=$(xwininfo -id ${window_id} | awk '/Height:/ { print $2 }')

    local x_relative=$(xwininfo -id ${window_id} | awk '/Relative upper-left X:/ { print $4 }')
    local y_relative=$(xwininfo -id ${window_id} | awk '/Relative upper-left Y:/ { print $4 }')

    echo $(( ${height} + ${y_relative} + ${x_relative} ))
}


############################################################
function window.getMonitorId() {
    for (( i = 0; i < ${nmonitors}; ++i )); do
        if    [[ ${window[x]} -ge ${monitors[${i}, left]} ]] \
           && [[ ${window[x]} -lt ${monitors[${i}, right]} ]]; then
            echo "${i}"
            return
        fi
    done
}


############################################################
### \brief Move window inside a monitor.
### \1 Monitor id.
### \2 The X coordinate relative to the monitor.
### \3 The Y coordinate relative to the monitor. 
############################################################
function window.move() {
    window[x]=$(brush::calc "${monitors[${1}, left]} + ${2}")
    window[y]=$(brush::calc "${monitors[${1}, top]} + ${3}")

    # wmctrl -ir "${window[id]}" -e <G>,<X>,<Y>,<W>,<H>
    brush::log.d "wm-ctrl.window.move()" "wmctrl -ir ${window[id]} -e 0,${window[x]},${window[y]},-1,-1"
    wmctrl -ir "${window[id]}" -e "0,${window[x]},${window[y]},-1,-1"
}


############################################################
### \brief Resize window inside a monitor.
### \1 The width.
### \2 The height. 
############################################################
function window.resize() {
    local width=${1}
    local height=${2}

    [[ ${width} -ne -1 ]] && window[width]=${width}
    [[ ${height} -ne -1 ]] && window[height]=${height}

    # wmctrl -ir "${window[id]}" -e <G>,<X>,<Y>,<W>,<H>
    brush::log.d "wm-ctrl.window.resize()" "wmctrl -ir "${window[id]}" -e 0,-1,-1,${width},${height}"
    wmctrl -ir "${window[id]}" -e "0,-1,-1,${width},${height}"
}


############################################################
function window.moveToMonitorCenter() {
    local monitor_id=$(window.getMonitorId)

    window.move ${monitor_id}                                                           \
                $(( ${monitors[${monitor_id}, half_width]} - ${window[half_width]} ))   \
                $(( ${monitors[${monitor_id}, half_height]} - ${window[half_height]} ))
}


############################################################
function window.getPercentX() {
    local monitor_id=${1}

    brush::percentTo $((${window[x]} - ${monitors[${monitor_id}, left]})) ${monitors[${monitor_id}, width]}
}


############################################################
function window.getPercentY() {
    local monitor_id=${1}

    brush::percentTo $((${window[y]} - ${monitors[${monitor_id}, top]})) ${monitors[${monitor_id}, height]}
}


############################################################
function window.isMaximized() {
    [[ -n $(grep -o '_NET_WM_STATE_MAXIMIZED_HORZ, _NET_WM_STATE_MAXIMIZED_VERT' <<< "${window[prop]}") ]]
    echo $(( ${?} == 0 ))
}


############################################################
function window.maximize() {
    wmctrl -ir ${window[id]} -b add,maximized_horz,maximized_vert
}


############################################################
function window.unmaximize() {
    wmctrl -ir ${window[id]} -b remove,maximized_horz,maximized_vert
}


############################################################
declare window_is_maximized=0

function window.pushProperties() {
    if (( $(window.isMaximized) )); then
        window_is_maximized=1
        window.unmaximize
        brush::log.i "wr-ctrl.window.pushProperties()" "window is 'maximized'"
    fi
}


############################################################
function window.popProperties() {
    (( window_is_maximized )) && window.maximize
}


############################################################
function window.moveToMonitor() {
    local direction=${1}
    local from_monitor_id="$(window.getMonitorId)"
    local to_monitor_id=${monitors_map[${from_monitor_id}, ${direction}]}

    # return if we are on the same monitor.
    brush::log.d "wm-ctrl.window.moveToMonitor()" "from_monitor_id = ${from_monitor_id}"
    brush::log.d "wm-ctrl.window.moveToMonitor()" "to_monitor_id = ${to_monitor_id}"
    if  [[ -z "${to_monitor_id}" ]] || [[ ${from_monitor_id} == ${to_monitor_id} ]]; then
        return
    fi

    local x=$(brush::percentOf $(window.getPercentX ${from_monitor_id}) ${monitors[${from_monitor_id}, width]})
    brush::log.d "wm-ctrl.window.moveToMonitor()" "window new x = ${x}"

    local y=$(brush::percentOf $(window.getPercentY ${from_monitor_id}) ${monitors[${from_monitor_id}, height]})
    brush::log.d "wm-ctrl.window.moveToMonitor()" "window new y = ${y}"

    # push properties and remove them to be able to move (tile, maximized).
    window.pushProperties

    # move to target monitor.
    window.move ${to_monitor_id} "${x}" "${y}"

    # pop properties and add them (tile, maximized).
    window.popProperties
}

# window to manage it.
declare -A window=([id]=$(xdotool getactivewindow)
                   [x]=$(window.getX ${window[id]})
                   [y]=$(window.getY ${window[id]})
                   [width]=$(window.getWidth ${window[id]})
                   [half_width]=$(( ${window[width]} / 2 ))
                   [height]=$(window.getHeight ${window[id]})
                   [half_height]=$(( ${window[height]} / 2 ))
                   [prop]=$(xprop -id ${window[id]} | egrep -o '_NET_WM_STATE\w+,?' | tr '\n' ' ')
                  )

brush::log.d "wm-ctrl.global" "window = {
                                  [id]          = ${window[id]}
                                  [x]           = ${window[x]}
                                  [y]           = ${window[y]}
                                  [width]       = ${window[width]}
                                  [half_width]  = ${window[half_width]}
                                  [height]      = ${window[height]}
                                  [half_height] = ${window[half_height]}
                              }"


#**************************************************************************************************#


############################################################
function main() {
    brush::addOption '-c'                        \
                     '--center'                  \
                     'window.moveToMonitorCenter'

    brush::addOption '-lm'                      \
                     '--to-left-monitor'        \
                     'window.moveToMonitor left'

    brush::addOption '-rm'                       \
                     '--to-right-monitor'        \
                     'window.moveToMonitor right'

    brush::manageArguments "${@}"
}

# run main.
main "${@}"
